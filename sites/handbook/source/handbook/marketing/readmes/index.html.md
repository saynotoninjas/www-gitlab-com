---
layout: handbook-page-toc
title: GitLab Marketing Team READMEs
description: "Learn more about working with various members of the marketing team"
---
## Marketing Team READMEs

- [Darren M.'s README (Head of Remote)](/handbook/marketing/readmes/dmurph/)
- [William Chia's README (Sr. PMM)](/handbook/marketing/readmes/william-chia.html)
- [Parker Ennis README (Sr. PMM)](/handbook/marketing/readmes/parker-ennis.html)
- [Wil Spillane's README  (Head of Social)](https://about.gitlab.com/handbook/marketing/readmes/wspillane.html)
- [Michael Preuss’ README (Senior Manager, Digital Experience)](https://about.gitlab.com/handbook/marketing/readmes/michael-preuss.html)
- [Tyler Williams' README (Fullstack Engineer)](https://about.gitlab.com/handbook/marketing/readmes/tyler-williams.html) 
- [Sanmi Ayotunde Ajanaku's README (Frontend Engineer)](https://about.gitlab.com/handbook/marketing/readmes/sanmiayotunde.html)
- [Lauren Barker's README (Fullstack Engineer)](https://about.gitlab.com/handbook/marketing/readmes/lauren-barker.html)
- [Laura Duggan's README (Frontend Engineer)](https://about.gitlab.com/handbook/marketing/readmes/laura-duggan.html)
- [Nathan Dubord's README (Frontend Engineer)](https://about.gitlab.com/handbook/marketing/readmes/nathan-dubord.html)
